<?php
/**
 * Created by PhpStorm.
 * User: mateng马腾
 * Date: 2019/2/19
 * Time: 上午9:17
 * Title:
 * Description:
 */
class DemoPlugin extends Yaf_Plugin_Abstract {

    // 在路由之前触发（这个是7个事件中, 最早的一个. 但是一些全局自定的工作, 还是应该放在Bootstrap中去完成）
    public function routerStartup(Yaf_Request_Abstract $request, Yaf_Response_Abstract $response) {

        // vd($_GET);

    }

    public function routerShutdown(Yaf_Request_Abstract $request, Yaf_Response_Abstract $response) {
    }

    public function dispatchLoopStartup(Yaf_Request_Abstract $request, Yaf_Response_Abstract $response) {
    }

    public function preDispatch(Yaf_Request_Abstract $request, Yaf_Response_Abstract $response) {
    }

    public function postDispatch(Yaf_Request_Abstract $request, Yaf_Response_Abstract $response) {
    }

    public function dispatchLoopShutdown(Yaf_Request_Abstract $request, Yaf_Response_Abstract $response) {
    }
}