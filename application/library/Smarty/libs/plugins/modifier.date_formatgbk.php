<?php
/**
 * Smarty plugin
 *
 * @package    Smarty
 * @subpackage PluginsModifier
 */

/**
 * Smarty date_format modifier plugin
 * Type:     modifier
 * Name:     date_format
 * Purpose:  format datestamps via strftime
 * Input:
 *          - string: input date string
 *          - format: strftime format for output
 *          - default_date: default date if $string is empty
 *
 * @link   http://www.smarty.net/manual/en/language.modifier.date.format.php date_format (Smarty online manual)
 * @author Monte Ohrt <monte at ohrt dot com>
 *
 * @param string $string       input date string
 * @param string $format       strftime format for output
 * @param string $default_date default date if $string is empty
 * @param string $formatter    either 'strftime' or 'auto'
 *
 * @return string |void
 * @uses   smarty_make_timestamp()
 */
function smarty_modifier_date_formatgbk($string, $format = '%b %e, %Y', $default_date = '')
{

    // if (substr(PHP_OS,0,3) == 'WIN') {
    //     $_win_from = array ('%e',   '%T',        '%D');
    //     $_win_to    = array ('%#d', '%H:%M:%S', '%m/%d/%y');
    //     $format = str_replace($_win_from, $_win_to, $format);
    // }
    //
    // $arrTemp = array('年','月','日','时','分','秒','?r');
    // foreach($arrTemp as $v){
    //     if(strpos($format,$v)){
    //         $strFormat = str_replace('%','',$format);
    //     }
    // }
    //
    // if(empty($string)) {
    // //     if(!emptyempty($strFormat)) return date($strFormat, smarty_make_timestamp($string));
    // //     else return strftime($format, smarty_make_timestamp($string));
    // // } elseif (isset($default_date) && $default_date != '') {
    // //     if(!emptyempty($strFormat)) return date($strFormat, smarty_make_timestamp($default_date));
    // //     else return strftime($format, smarty_make_timestamp($default_date));
    //     return date($strFormat,time());
    // } else {
        date_default_timezone_set('PRC');
        return date($format,$string);
    // }

}
