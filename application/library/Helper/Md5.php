<?php
/**
 * Created by PhpStorm.
 * User: mateng马腾
 * Date: 2018/5/8
 * Time: 上午9:10
 */
class Helper_Md5 {

    /**
     * 明文密码
     * @param $password
     * @return string
     */
    public static function password($password){

        return md5(md5($password.Yaf_Registry::get("config")->salt));
    }
}