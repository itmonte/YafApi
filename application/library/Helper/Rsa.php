<?php
/**
 * Created by PhpStorm.
 * User: mateng马腾
 * Date: 2018/4/18
 * Time: 下午1:25
 */

class Helper_Rsa
{
    private static $PRIVATE_KEY = '';//私钥
    private static $PUBLIC_KEY = '';//公钥

    /*****************************核心*************************************/
    /** * 获取私钥 * @return bool|resource */
    private static function getPrivateKey()
    {
        $privKey = self::$PRIVATE_KEY;
        return openssl_pkey_get_private($privKey);
    }
    /** * 获取公钥 * @return bool|resource */
    private static function getPublicKey()
    {
        $publicKey = self::$PUBLIC_KEY;
        return openssl_pkey_get_public($publicKey);
    }
    /** * 私钥加密 * @param string $data * @return null|string */
    public static function privEncrypt($data = '')
    {
        if (!is_string($data)) {
            return null;
        }
        return openssl_private_encrypt($data,$encrypted,self::getPrivateKey()) ? base64_encode($encrypted) : null;
    }
    /** * 公钥加密 * @param string $data * @return null|string */
    public static function publicEncrypt($data = '')
    {
        if (!is_string($data)) {
            return null;
        }
        return openssl_public_encrypt($data,$encrypted,self::getPublicKey()) ? base64_encode($encrypted) : null;
    }
    /** * 私钥解密 * @param string $encrypted * @return null */
    public static function privDecrypt($encrypted = '')
    {
        if (!is_string($encrypted)) {
            return null;
        }
        return (openssl_private_decrypt(base64_decode($encrypted), $decrypted, self::getPrivateKey())) ? $decrypted : null;
    }
    /** * 公钥解密 * @param string $encrypted * @return null */
    public static function publicDecrypt($encrypted = '')
    {
        if (!is_string($encrypted)) {
            return null;
        }
        return (openssl_public_decrypt(base64_decode($encrypted), $decrypted, self::getPublicKey())) ? $decrypted : null;
    }
}