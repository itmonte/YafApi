<?php
/**
 * Created by PhpStorm.
 * User: mateng马腾
 * Date: 2018/8/25
 * Time: 上午10:15
 */

class Helper_Session {

    /**
     * @param $name
     * @return mixed
     * 检查session
     */
    public static function has($name){
        return Yaf_Session::getInstance()->has($name);
    }

    /**
     * @param $name
     * @param $value
     * @return mixed
     * 写入session
     */
    public static function set($name,$value){
        return Yaf_Session::getInstance()->set($name,$value);
    }

    /**
     * @param $name
     * @return mixed
     * 获取session
     */
    public static function get($name){
        return Yaf_Session::getInstance()->get($name);
    }

    /**
     * @param $name
     * @return mixed
     * 删除session
     */
    public static function del($name){
        return Yaf_Session::getInstance()->del($name);
    }

}