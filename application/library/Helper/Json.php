<?php
/**
 * Created by PhpStorm.
 * User: mateng马腾
 * Date: 2018/4/18
 * Time: 上午11:16
 */
class Helper_Json
{
    /**返回json以及请求头
     * @param $array
     */
    public static function output($array)
    {
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
        header('Access-Control-Allow-Methods: GET, POST, PUT');
        header('Access-Control-Allow-Origin: *');//解决跨域
        header('Content-type: application/json');
        exit(json_encode($array));
    }

    /**成功返回json
     * @param $array
     * @param bool $isSuccess
     */
    public static function outputNormal($data = "", $message = "成功",$code=1)
    {
        $outArray = ['isSuccess'=>true, 'code'=>$code, 'response'=>$data, 'message'=>$message,'ts'=>time()];
        self::output($outArray);
    }

    /**失败返回json
     * @param $reason
     * @param int $code
     */
    public static function outputError($message = "失败",$code=0)
    {
        $outArray = ['isSuccess'=>false, 'code'=>$code,'message'=>$message,'ts'=>time()];
        self::output($outArray);
    }

}