<?php
/**
 * Created by PhpStorm.
 * User: mateng马腾
 * Date: 2018/9/1
 * Time: 上午11:25
 */

// Db::table("user")->where(['id'=>1])->getShow();
// vd(Db::table("user")->where(['id'=>2])->updates(['time'=>'2011']));
// vd(Db::table("user")->insertAdd([['username'=>'perma','password'=>'123123','time'=>'2018-9-9'],['username'=>'perma','password'=>'123123','time'=>'2018-9-9']]));
// vd(Db::table("user")->where(['id'=>1])->del());

class Db_Tab extends Db_Medoo{

    public $_table;

    public $whereArray;

    public $fields;

    public $joins;

    public $orders;

    public $limits;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return mixed
     * 单条查询
     */
    public function getShow(){
        // 自定义字段
        if (!empty($this->fields)){
            $columnss = $this->fields;
        }else{
            $columnss = "*";
        }
        $data = $this->get($this->_table, $columnss,$this->whereArray);

        // return $this->log();
        return $data;

    }
    /**
     * @return mixed
     * 多条查询
     */
    public function getList(){

        $where = [];

        if (!empty($this->whereArray)){
            $where['AND'] = $this->whereArray;
        }

        // 分页
        if (!empty($this->limits)){
            $where['LIMIT'] = $this->limits;
        }

        // 排序
        if (!empty($this->orders)){
            $where['ORDER'] = $this->orders;
        }

        // 自定义字段
        if (!empty($this->fields)){
            $columnss = $this->fields;
        }else{
            $columnss = "*";
        }

        if (!empty($this->joins)){
            $data = $this->select($this->_table, $this->joins, $columnss ,$where);
        }else{
            $data = $this->select($this->_table, $columnss,$where);
        }

        // return $this->log();
        return $data;

    }

    /**
     * 总数查询
     */
    public function getCount(){

        $where = [];

        if (!empty($this->whereArray)){
            $where['AND'] = $this->whereArray;
        }

        $data = $this->count($this->_table, "*",$where);

        return $data;

    }

    /**
     * @param $data
     * @return array|mixed
     * 数据添加
     */
    public function insertAdd($data){
        $last = $this->insert($this->_table,$data);

        // return $this->log();
        return $last;
    }

    /**
     * @param $data
     * @return bool
     * 修改数据
     */
    public function updates($data){

        $r = $this->update($this->_table,$data,$this->whereArray);

        // return $this->log();
        return $r;
    }

    /**
     * 删除数据
     */
    public function del(){

        return $this->delete($this->_table,$this->whereArray);

    }

    /**
     * @return bool
     * 查询用户是否存在
     */
    public function getHas(){

        $where['AND'] = $this->whereArray;

        $data = $this->has($this->_table, $where);

        return $data;
    }

    public function field($fields = []){

        $this->fields = $fields;

        return $this;
    }

    public function where($whereArray = []) {

        $this->whereArray = $whereArray;

        return $this;
    }

    public function join($joins = []) {

        $this->joins = $joins;

        return $this;
    }

    public function order($orders = []) {

        $this->orders = $orders;

        return $this;
    }

    public function limit($limits = []) {

        $this->limits = $limits;

        return $this;
    }

    private function name($tableName){

        $this->_table = $tableName;

        return $this;
    }

    /**
     * @param $name
     * @param $arguments
     * 模拟静态方法
     */
    public function __callStatic($name,$arguments){

        return call_user_func([new Db_Tab(),'name'],$arguments[0]);

    }

}




