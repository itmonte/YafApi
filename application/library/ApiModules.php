<?php
/**
 * Created by PhpStorm.
 * User: mateng
 * Date: 2017/12/31
 * Time: 上午2:04
 */

class ApiModules extends Yaf_Controller_Abstract
{
    public function init()
    {
        // 关闭模板自动渲染
        Yaf_Dispatcher::getInstance()->disableView();
    }

    /**
     * get数据接受
     * @param string $name 数据名称
     * @param string $type 类型默认int
     * @param bool $must 是否必填默认false
     */
    public function getQuery($name = null, $type = 'int', $must = false, $msg = null){

        $getName = '';

        $get_name = $this->getRequest()->get($name);

        if (empty($get_name)){
            if ($must == true) {
                Helper_Json::outputError($msg);
            }else{
                $getName = 0;
            }
        }else{
            $getName = $get_name;
        }

        switch ($type)
        {
            case 'int':
                $getName = (int)$getName;
                break;
            case 'string':
                $getName = htmlspecialchars((string)$getName);
                break;
            case 'float':
                $getName = (float)$getName;
                break;
            default:
                $getName = (string)$getName;

        }

        return $getName;

    }

    /**
     * @param string $name 数据名称
     * @param string $type 类型默认int
     * @param bool $must 是否必填默认false
     */
    public function postQuery($name = '', $type = 'int', $must = false, $msg = null){

        $postName = '';

        $post_name = $this->getRequest()->getPost($name);

        if (empty($post_name)){
            if ($must == true) {
                Helper_Json::outputError($msg);
            }else{
                $postName = 0;
            }
        }else{
            $postName = $post_name;
        }

        switch ($type)
        {
            case 'int':
                $postName = (int)$postName;
                break;
            case 'string':
                $postName = htmlspecialchars((string)$postName);
                break;
            case 'float':
                $postName = (float)$postName;
                break;
            default:
                $postName = (string)$postName;

        }

        return $postName;

    }
}