<?php
//+-------------------------------------------------------
//|   author:雪域       QQ:2848588063     图像处理类库
//+-------------------------------------------------------
require_once ('lib/Image.class.php');
class Common_Img_Main
{
    /**
     * @param $beforeImg    缩放之前的图片,包括图片路径
     * @param $afterImg     生成后的缩略图名称.包含路径
     * @param $width        生成缩略图的最大宽度
     * @param $height       生成缩略图的最大高度
     * @param $type         生成缩略图类型,默认采用原图等比缩放.可直接填写相对的数字1,2,3,4等.
     * IMAGE_THUMB_SCALE     =   1 ; //等比例缩放类型
     * IMAGE_THUMB_FILLED    =   2 ; //缩放后填充类型
     * IMAGE_THUMB_CENTER    =   3 ; //居中裁剪类型
     * IMAGE_THUMB_NORTHWEST =   4 ; //左上角裁剪类型
     * IMAGE_THUMB_SOUTHEAST =   5 ; //右下角裁剪类型
     * IMAGE_THUMB_FIXED     =   6 ; //固定尺寸缩放类型
     */
    public static function thumb($beforeImg,$afterImg,$width=150,$height=150,$type = Image::IMAGE_THUMB_SCALE)
    {
        $img = new Image();
        return $img->open($beforeImg)->thumb($width,$height,$type)->save($afterImg);
    }

    /**
     * @param $beforeImg        图像裁剪之前的原图片名称以及路径
     * @param $afterImg         图像裁剪之后的图片名称以及路径
     * @param $w                图片裁剪区域的宽度
     * @param $h                图片裁剪区域的高度
     * @param int $x            图片裁剪x坐标
     * @param int $y            图片裁剪Y坐标
     * @param null $width       图片保存的宽度
     * @param null $height      图片保存的高度
     */
    public static function crop($beforeImg,$afterImg,$w,$h,$x=0,$y=0,$width=null,$height=null)
    {
        $img = new Image();
        return $img->open($beforeImg)->crop($w, $h, $x, $y, $width, $height)->save($afterImg);
    }

    /**
     * @param $beforeImg        生成水印之前的图片名称
     * @param $afterImg         生成水印之后的图片名称
     * @param $waterImg         水印图片名称路径
     * @param $locate           生成水印的位置 默认右下角,可直接填写相对的数字1,2,3,4等.
     * @param int $alpha        生成水印的透明度
     * IMAGE_WATER_NORTHWEST =   1 ; //左上角水印
     * IMAGE_WATER_NORTH     =   2 ; //上居中水印
     * IMAGE_WATER_NORTHEAST =   3 ; //右上角水印
     * IMAGE_WATER_WEST      =   4 ; //左居中水印
     * IMAGE_WATER_CENTER    =   5 ; //居中水印
     * IMAGE_WATER_EAST      =   6 ; //右居中水印
     * IMAGE_WATER_SOUTHWEST =   7 ; //左下角水印
     * IMAGE_WATER_SOUTH     =   8 ; //下居中水印
     * IMAGE_WATER_SOUTHEAST =   9 ; //右下角水印
     */
    public static function water($beforeImg,$afterImg,$waterImg,$locate = Image::IMAGE_WATER_SOUTHEAST,$alpha=80)
    {
        $img = new Image();
        return $img->open($beforeImg)->water($waterImg,$locate,$alpha)->save($afterImg);
    }

    /**
     * base64图片上传方法
     * @param $base64_img base64图片流
     * @param $new_filename 图片名称
     * @param $up_dir 图片存放路径
     * @return array
     */
    public static function base64Img($base64_img,$new_filename,$up_dir){
        $base64_img = trim($base64_img); //数据流

        $new_filename = trim($new_filename); //图片名字

        $up_dir = trim($up_dir); //保存目录

        if(!file_exists($up_dir)){
            mkdir($up_dir,0777);
        }
        if(preg_match('/^(data:\s*image\/(\w+);base64,)/', $base64_img, $result)){
            $type = $result[2];
            if(in_array($type,array('pjpeg','jpeg','jpg','gif','bmp','png'))){
                $new_file = $up_dir.$new_filename.'.'.$type;
                // return $new_file;
                if(file_put_contents($new_file, base64_decode(str_replace($result[1], '', $base64_img)))){
                    $img_path = str_replace('../../..', '', $new_file);
                    return ['success'=>true,'data'=>$new_filename.'.'.$type];
                }else{
                    return ['success'=>false,'data'=>'图片上传失败'];
                }
            }else{
                return ['success'=>false,'data'=>'图片上传类型错误'];
            }
        }else{
            return ['success'=>false,'data'=>'文件错误'];
        }
    }

}

    /**
     * Public_Img_Main::thumb('./img/banner3.png','./img/banner3-thumb.png');
     * Public_Img_Main::crop('./img/banner4.png','./img/banner4-crop.png',400,400);
     * Public_Img_Main::water('./img/banner5.png','./img/banner5-water.png','./img/phone.png');
     */

?>