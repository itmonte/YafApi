<?php
require_once ('lib/phpqrCode/phpqrcode.php');
class Common_Macode_Makecode
{

    static function  Newma($value)
    {

        $errorCorrectionLevel = 'L';//容错级别
        $matrixPointSize = 6;//生成图片大小
        QRcode::png($value, 'qrcode.png', $errorCorrectionLevel, $matrixPointSize, 2);
        $result='<img class="erweima" src="/qrcode.png">';
        return $result;
    }

}