<?php
//这是公用的jssdk类文件
class JSSDK {
  private $appId;//定义appid
  private $appSecret;//定义appsecret
  public function __construct($appId, $appSecret) {//初始化赋值
	$this->appId 		= $appId;
	$this->appSecret 	= $appSecret;
	$this->times 		= 3000;
  }

  public function GetSignPackage() {//定义一个获取微信接口参数的方法
	$protocol	= (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
	$url		= "$protocol$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
  	$seed		= $this->appId.md5($url);
  
	$result		= $this->getuserfile($seed);
	if(@$result->expire_last > time()){
		$signPackage = (array)$result;
	}else{
		$jsapiTicket= $this->getJsApiTicket();
		$protocol	= (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
		$url		= "$protocol$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		$timestamp	= time();//当前时间戳
		$nonceStr	= $this->createNonceStr();
		$string 	= "jsapi_ticket=$jsapiTicket&noncestr=$nonceStr&timestamp=$timestamp&url=$url";
		$signature	= sha1($string);
	
		$signPackage = array(//将结果集写入到数组里
		  "appId"     => $this->appId,
		  "nonceStr"  => $nonceStr,
		  "timestamp" => $timestamp,
		  "url"       => $url,
		  "signature" => $signature,
		  "rawString" => $string,
		  "expire_last" => time()+ $this->times
		);
		
		$userfile = fopen('../jssdk/'.$seed.".json", "w") or die("Unable to open file!");//打开json文件
		fwrite($userfile, json_encode($signPackage));//将结果转换为json数据
		fclose($userfile);
	}
    return $signPackage; 
  }

  private function getuserfile($seed = ''){
	$data = @json_decode(file_get_contents(('../jssdk/'.$seed.".json")));//获取json数据的内容
    return $data; 
  }


  private function createNonceStr($length = 16) {
    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    $str = "";
    for ($i = 0; $i < $length; $i++) {
      $str .= substr($chars, mt_rand(0, strlen($chars) - 1), 1);
    }
    return $str;
  }

  private function getJsApiTicket() {
    // jsapi_ticket 应该全局存储与更新，以下代码以写入到文件中做示例
    $data = json_decode(@file_get_contents("jsapi_ticket.json"));
    if (@$data->expire_time < time()) {
      $accessToken = $this->getAccessToken();
      // 如果是企业号用以下 URL 获取 ticket
      // $url = "https://qyapi.weixin.qq.com/cgi-bin/get_jsapi_ticket?access_token=$accessToken";
      $url = "https://api.weixin.qq.com/cgi-bin/ticket/getticket?type=jsapi&access_token=$accessToken";
      $res = json_decode($this->httpGet($url));
      $ticket = $res->ticket;
      if ($ticket) {
        @$data->expire_time = time() + 100;
        @$data->jsapi_ticket = $ticket;     
      }
    } else {
      $ticket = $data->jsapi_ticket;
    }

    return $ticket;
  }

  private function getAccessToken() {
    // access_token 应该全局存储与更新，以下代码以写入到文件中做示例
    $data = json_decode(@file_get_contents("access_token.json"));
    if (@$data->expire_time < time()) {
      // 如果是企业号用以下URL获取access_token
      // $url = "https://qyapi.weixin.qq.com/cgi-bin/gettoken?corpid=$this->appId&corpsecret=$this->appSecret";
      $url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=$this->appId&secret=$this->appSecret";
      $res = json_decode($this->httpGet($url));
      $access_token = $res->access_token;
      if ($access_token) {
        @$data->expire_time = time() + 100;
        @$data->access_token = $access_token;       
      }
    } else {
      $access_token = $data->access_token;
    }
    return $access_token;
  }

  private function httpGet($url) {
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_TIMEOUT, 500);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($curl, CURLOPT_URL, $url);

    $res = curl_exec($curl);
    curl_close($curl);

    return $res;
  }
}
