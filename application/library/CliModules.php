<?php
/**
 * Created by PhpStorm.
 * User: mateng马腾
 * Date: 2018/8/7
 * Time: 下午4:40
 */
class CliModules extends Yaf_Controller_Abstract
{
    public function init()
    {
        if ($_SERVER['PHP_SELF'] != 'cli.php') {
            die('只能在CLI模式中使用');
        }
        // 关闭模板自动渲染
        Yaf_Dispatcher::getInstance()->disableView();
    }

}