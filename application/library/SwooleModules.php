<?php
/**
 * Created by PhpStorm.
 * User: mateng马腾
 * Date: 2018/8/8
 * Time: 下午2:10
 */
class SwooleModules extends Yaf_Controller_Abstract
{
    public function init()
    {

        if ($_SERVER['PHP_SELF'] != 'swoole.php') {
            die('只能在SwooleCLI模式中使用');
        }
        // 关闭模板自动渲染
        Yaf_Dispatcher::getInstance()->disableView();
    }

}