<?php
/**
 * Created by PhpStorm.
 * User: mateng马腾
 * Date: 2018/8/25
 * Time: 上午8:56
 */
class BaseController extends Yaf_Controller_Abstract
{
    // public $rpcHost;//User微服务

    public function init()
    {
        // $this->rpcHost = Yaf_Registry::get("rpcConfig")->rpc->host;//微服务地址
    }

    /**
     * get数据接受
     * @param string $name 数据名称
     * @param string $type 类型默认int
     */
    public function getQuery($name = '', $type = 'string', $must = false, $msg = null){

        $get_name = $this->getRequest()->get($name);

        if (empty($get_name)){
            if ($must == true) {
                Helper_Json::outputError($msg);
            }else{
                $getName = "";
            }
        }else{
            $getName = $get_name;
        }

        switch ($type)
        {
            case 'int':
                $getName = (int)$getName;
                break;
            case 'string':
                $getName = htmlspecialchars((string)$getName);
                break;
            case 'float':
                $getName = (float)$getName;
                break;
            default:
                $getName = htmlspecialchars((string)$getName);
        }

        //日志记录
        //SeasLog::info('get请求，参数:'.$name.'('.$getName.'),类型:'.$type);
        return $getName;

    }

    /**
     * @param string $name 数据名称
     * @param string $type 类型默认int
     */
    public function postQuery($name = '', $type = 'string', $must = false, $msg = null){

        $post_name = $this->getRequest()->getPost($name);

        if (empty($post_name)){
            if ($must == true) {
                Helper_Json::outputError($msg);
            }else{
                $postName = "";
            }
        }else{
            $postName = $post_name;
        }

        switch ($type)
        {
            case 'int':
                $postName = (int)$postName;
                break;
            case 'string':
                $postName = htmlspecialchars((string)$postName);
                break;
            case 'float':
                $postName = (float)$postName;
                break;
            default:
                $postName = htmlspecialchars((string)$postName);

        }

        //日志记录
        // SeasLog::info('post请求，参数:'.$name.'('.$getName.'),类型:'.$type);
        return $postName;

    }

}