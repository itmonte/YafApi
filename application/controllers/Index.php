<?php
/**
 * @name IndexController
 * @author mateng
 * @desc 默认控制器
 * @see http://www.php.net/manual/en/class.yaf-controller-abstract.php
 */

class IndexController extends BaseController {

	/** 
     * 默认动作
     * Yaf支持直接把Yaf_Request_Abstract::getParam()得到的同名参数作为Action的形参
     * 对于如下的例子, 当访问http://yourhost/lbxdapi/index/index/index/name/mateng 的时候, 你就会发现不同
     *
     */
    public function init()
    {
        parent::init();
    }

    public function indexAction(){




        // $mzData = Db_Tab::name("mz")->where(['id'=>10])->getShow();//单调数据
        // $mzData = Db_Tab::name("mz")->where()->order(["id"=>"ASC"])->limit([0,2])->getList();//多条数据
        // $ints = Db_Tab::name("mz")->insertAdd($data);//数据的添加
        // $mzUpdate = Db_Tab::name("mz")->where(['id'=>58])->updates($data);//数据编辑
        // $del = Db_Tab::name("mz")->where(["id"=>58])->del();//删除数据

        // Helper_Json::outputNormal($mzData,"恭喜你民族列表显示成功");//接口数据的返回（正确的）
        // Helper_Json::outputError("数据不存在");//接口数据的返回（错误的）

        // new Common_World_Main();//第三方类实例调用
        // Common_World_Main::abs("1222223");//第三方类静态调用

        // Helper_Rsa::privDecrypt("");//rsa解密
        // Helper_Rsa::privEncrypt("");//rsa加密

        // Helper_Session::set("token","hfghjfuiert8973452647jhgfsgyerut");//写入session
        // Helper_Session::get("token");//获取session

        // php cli.php request_uri="/Cli/Test/index" //cli模式运行 cli模块下的test控制器下的index方法


        $this->getView()->assign("text","Hello,mtYaf-Smarty v1.0");

        $this->getView()->display("index/index.html");

        return TRUE;
    }


    public function testAction(){

        $this->getView()->assign("name", "123");

        $this->getView()->display("index/test.html");
        // $Demo_Demo = new Demo_Demo();

        // vd($Demo_Demo->a("Monte", 0));

        // vd(Demo_Img_Demo::b());


        // $adminData = Db_Tab::name("admin")->where()->getShow();//单条查询
        // $adminData = Db_Tab::name("admin")->where()->getList();//多条查询

        // $userData = [];
        //
        // $userData['username'] = 'Monte';
        // $userData['password'] = '123';

        // $int = Db_Tab::name("admin")->insertAdd($userData);// 添加一条数据
        // $uId = Db_Tab::name("admin")->where(['id'=>6])->updates(['username'=>'1111111']);//修改一条数据

        // $dId = Db_Tab::name("admin")->where(['id'=>6])->del();


        // vd($dId);

    }

}
