<?php
/**
 * @name Bootstrap
 * @author mateng
 * @desc 所有在Bootstrap类中, 以_init开头的方法, 都会被Yaf调用,
 * @see http://www.php.net/manual/en/class.yaf-bootstrap-abstract.php
 * 这些方法, 都接受一个参数:Yaf_Dispatcher $dispatcher
 * 调用的次序, 和申明的次序相同
 */

class Bootstrap extends Yaf_Bootstrap_Abstract {

    public function _initConfig() {
        // 开启session
        // Yaf_Session::getInstance()->start();
		//把配置保存起来
		$arrConfig = Yaf_Application::app()->getConfig();
        Yaf_Registry::set('config', $arrConfig);
	}

    //注册静态配置文件
    public function _initConfigOther() {
        // 保存rpc.ini配置文件
        // $rpcConfig = new Yaf_Config_Ini(APPLICATION_PATH.'conf/rpc.ini','common');
        // Yaf_Registry::set('rpcConfig', $rpcConfig);
        // $imgConfig = new Yaf_Config_Ini(APPLICATION_PATH.'conf/img.ini','common');
        // Yaf_Registry::set('imgConfig', $imgConfig);
    }

	public function _initPlugin(Yaf_Dispatcher $dispatcher) {
		//注册一个插件
		$objSamplePlugin = new SamplePlugin();
		$dispatcher->registerPlugin($objSamplePlugin);
		// 注册两个插件
        $objDemoPlugin = new DemoPlugin();
        $dispatcher->registerPlugin($objDemoPlugin);

	}

	public function _initRoute(Yaf_Dispatcher $dispatcher) {
		//在这里注册自己的路由协议,默认使用简单路由
	}

    public function _initView(Yaf_Dispatcher $dispatcher){
        //在这里注册自己的view控制器，例如smarty,firekylin
        Yaf_Dispatcher::getInstance()->disableView(); //因为要用smarty引擎作为渲染，所以关闭yaf自身的自动渲染功能
    }

    public function _initSmarty(Yaf_Dispatcher $dispatcher) {
        $smarty = new Smarty_Adapter(null, Yaf_Application::app()->getConfig()->smarty);//实例化引入文件的类
        $dispatcher->getInstance()->setView($smarty);//渲染模板
    }

	// 注册一个seaslog日志
    public function _initSeaslog(){
        //seaslog路径设置
        SeasLog::setBasePath(APPLICATION_PATH.'log');
    }

    //注册一个360防止注入
    // public function _initCommonSafe(){
    //     Yaf_Loader::import(Yaf_Registry::get("config")->application->directory . '/common/safe3.php');
    // }

    //注册一个公共函数库
    public function _initCommonFunctions(){
        Yaf_Loader::import(Yaf_Registry::get("config")->application->directory . '/common/functions.php');
    }
}
