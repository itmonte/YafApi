<?php
/**
 * Created by PhpStorm.
 * User: mateng
 * Date: 2017/12/31
 * Time: 上午2:33
 */
class Api_TestModel extends medoo{

    private $_table="test";

    public function __construct() {
        parent::__construct();
    }

    public function selectApiTest() {
        $list=$this->select($this->_table,"*");
        return $list;
    }

}

