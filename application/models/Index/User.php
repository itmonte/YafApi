<?php
/**
 * Created by PhpStorm.
 * User: mateng马腾
 * Date: 2018/9/1
 * Time: 上午11:09
 */
class Index_UserModel extends Db_Medoo{

    private $_table="user";

    public function __construct() {
        parent::__construct();
    }

    public function selectIndexUser() {
        $list=$this->select($this->_table,"*");

        return $list;
    }

    /******************************************** 数据逻辑操作 ********************************************/

}