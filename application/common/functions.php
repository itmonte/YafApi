<?php
/**
 * Created by PhpStorm.
 * User: mateng马腾
 * Date: 2018/8/6
 * Time: 下午3:08
 */

/**
 * @param $url 程序地址
 * @param array $param post参数
 * @return bool
 * 异步执行程序(执行一些简单的异步处理)
 * 使用demo $param = ['123'];doRequest("http://mtyaf.mac.com/Api/Index/test1",$param);
 */
function doRequest($url, $param=array()){

    $urlinfo = parse_url($url);

    $host = $urlinfo['host'];
    $path = $urlinfo['path'];
    $query = isset($param)? http_build_query($param) : '';

    $port = 80;
    $errno = 0;
    $errstr = '';
    $timeout = 30;

    $fp = fsockopen($host, $port, $errno, $errstr, $timeout);

    $out = "POST ".$path." HTTP/1.1\r\n";
    $out .= "host:".$host."\r\n";
    $out .= "content-length:".strlen($query)."\r\n";
    $out .= "content-type:application/x-www-form-urlencoded\r\n";
    $out .= "connection:close\r\n\r\n";
    $out .= $query;

    fputs($fp, $out);
    fclose($fp);

    return true;
}

// 随机验证码生成
function generate_code($length = 4) {
    $min = pow(10 , ($length - 1));
    $max = pow(10, $length) - 1;
    return rand($min, $max);
}

/**************************************** 工具类函数 ********************************************/
/**
 * @param array $array
 * 断点调试数组var_dump
 */
function vd($str){
    echo "<pre>";
    var_dump($str);
    echo "</pre>";
    exit;
}

/**
 * @param array $array
 * 断点调试数组print_r
 */
function pr($str){
    echo "<pre>";
    print_r($str);
    echo "</pre>";
    exit;
}

