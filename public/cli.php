<?php
/**
 * Created by PhpStorm.
 * User: mateng马腾
 * Date: 2018/8/7
 * Time: 下午4:38
 */
define('APPLICATION_PATH', dirname(__FILE__)."/../");

$application = new Yaf_Application( APPLICATION_PATH . "/conf/application.ini");

$application->bootstrap()->getDispatcher()->dispatch(new Yaf_Request_Simple());

?>