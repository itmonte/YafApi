# YafApi

### 需要扩展
#### Yaf、Swoole、Seaslog

### 说明
#### 基于yaf写的一个api开发框架，使用smarty做的模版引擎，集成了Swoole，封装了Db_Tab数据操作类

### 更新信息

#### 更新数据库Db_Tab操作，优化目录结构


### 模块生成
#### php build 模块名
#### 注：生成完成后需要在conf/application.ini的application.modules下进行配置

### 操作手册

#### $mzData = Db_Tab::name("mz")->where(['id'=>10])->getShow();//单调数据
#### $mzData = Db_Tab::name("mz")->where()->order(["id"=>"ASC"])->limit([0,2])->getList();//多条数据
#### $ints = Db_Tab::name("mz")->insertAdd($data);//数据的添加
#### $mzUpdate = Db_Tab::name("mz")->where(['id'=>58])->updates($data);//数据编辑
#### $del = Db_Tab::name("mz")->where(["id"=>58])->del();//删除数据

#### Helper_Json::outputNormal($mzData,"恭喜你民族列表显示成功");//接口数据的返回（正确的）
#### Helper_Json::outputError("数据不存在");//接口数据的返回（错误的）

#### new Common_World_Main();//第三方类实例调用
#### Common_World_Main::abs("1222223");//第三方类静态调用

#### Helper_Rsa::privDecrypt("");//rsa解密
#### Helper_Rsa::privEncrypt("");//rsa加密

#### Helper_Session::set("token","hfghjfuiert8973452647jhgfsgyerut");//写入session
#### Helper_Session::get("token");//获取session

#### php cli.php request_uri="/Cli/Test/index" //cli模式运行 cli模块下的test控制器下的index方法


